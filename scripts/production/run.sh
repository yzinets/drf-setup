(
    # Wait for db start
    sleep 10 &&
    # Migrate
    pipenv run python manage.py migrate &&
    # Collect static
    pipenv run python manage.py collectstatic --noinput &&
    # Run
    pipenv run gunicorn -c settings/gunicorn.py settings.wsgi
)
