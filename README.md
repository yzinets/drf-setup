
# DRF Setup

Basic DRF (Django Rest Framework) setup
- DRF config  
- Configured CORS  
- Env file support (django-environ)  
- Custom pagination  
- Docker (docker-compose) with Postgres and NGINX setup  
