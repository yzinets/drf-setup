FROM python:3.7-alpine

# Sources
RUN mkdir -p /app

COPY ./Pipfile /app/
COPY ./Pipfile.lock /app/
WORKDIR /app

RUN apk add --update \
        bash linux-headers musl-dev \
        postgresql-dev python-dev \
        jpeg-dev zlib-dev gcc

RUN pip install pipenv
RUN pipenv install
