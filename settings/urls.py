"""settings URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from rest_framework.documentation import include_docs_urls
from rest_framework.schemas import get_schema_view


# API related

API_TITLE = 'DRF Setup'
API_DESCRIPTION = 'Basic DRF setup'
SCHEMA_VIEW = get_schema_view(
    title=API_TITLE,
    authentication_classes=(
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
    permission_classes=(
        'rest_framework.permissions.AllowAny',
        # 'rest_framework.permissions.IsAuthenticated',
        # 'rest_framework.permissions.IsAdminUser',
    ))


urlpatterns = [
    path('admin/', admin.site.urls),

    # DRF setup
    path('schema/', SCHEMA_VIEW),
    path('api-auth/', include(
        'rest_framework.urls',
        namespace='rest_framework')),
    path('docs/', include_docs_urls(
        title=API_TITLE,
        description=API_DESCRIPTION)),

    # API
    path('api/core', include('core.urls'))
]
