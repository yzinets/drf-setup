from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from django.http import HttpRequest


class CustomPagePagination(PageNumberPagination):
    page_size_query_param = 'page_size'

    def paginate_queryset(self, *args, **kwargs):
        self.request = args[1]  # type: HttpRequest
        return PageNumberPagination.paginate_queryset(self, *args, **kwargs)

    def get_paginated_response(self, data):
        # Extract page size
        page_size = self.request.GET.get(self.page_size_query_param)
        page_size = int(page_size) if page_size else self.page_size
        # Configure and return response
        return Response({
            'count': self.page.paginator.count,
            'pages': self.page.paginator.num_pages,
            'page': self.page.number,
            'page_next': self.page.next_page_number()
            if self.page.has_next()
            else None,
            'page_previous': self.page.previous_page_number()
            if self.page.has_previous()
            else None,
            'page_size': page_size,
            'results': data
        })
